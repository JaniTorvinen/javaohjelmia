import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Luvut {

    public static void main(String[] args) {
        
		Scanner lukija = new Scanner(System.in);
		ArrayList<Integer> luvut = new ArrayList<Integer>();	
		
		System.out.print("Kuinka monta lukua: "); 					
		int lkm = lukija.nextInt();

		for (int i=0; i<lkm; i++)
		{
			System.out.print("Anna luku: "); 		
			int luku = lukija.nextInt();
			luvut.add(luku); 
		}

		Collections.sort(luvut);  

		System.out.println(""); 					
		System.out.println("Tulostetaan luvut nousevassa järjestyksessä:"); 					

    		for (int i : luvut) 
		{
		      System.out.println(i);
    		}

    }

}