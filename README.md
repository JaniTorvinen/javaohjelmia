# JavaOhjelmia

Luvut.java on yksinkertainen ohjelma joka kysyy halutun määrän lukuja, järjestää ja tulostaa ne nousevaan järjestykseen.

Luvut käännetään .class tiedostoksi komennolla javac Luvut.java

Luvut ajetaan komennolla java Luvut

