import java.lang.Math;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;

public class Lotto {

    	public static void main(String[] args) {

	// Luodaan lista
	ArrayList<Integer> kaikki = new ArrayList<Integer>();

	// Lisätään listaan 39 numeroa
	for (int i = 1; i <= 39; i++) 
	{
    		kaikki.add(i);
	}

	// Sekoitetaan listan numerot
	Collections.shuffle(kaikki);

	// Luodaan lista oikeille numeroille
	ArrayList<Integer> oikeat = new ArrayList<Integer>();

	// Lisätään oikeat numerot listaan
	for (int i = 0; i < 7; i++) 
	{
    		oikeat.add(kaikki.get(i));
	}

	// Jarjestetaan listan numerot nousevasti
	Collections.sort(oikeat);

	// Tulostetaan oikeat numerot		
	System.out.println("Lottonumerot:");
	System.out.println(oikeat);	
}


}
